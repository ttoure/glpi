FROM debian:11.8
MAINTAINER Taliby TOURE <ttoure05@gmail.com>

# Eviter les questions pendant l'installation des paquets
ENV DEBIAN_FRONTEND noninteractive

# Installation d'apache et de php8.1 avec quelques extensions
RUN apt update \
&& apt install --yes ca-certificates apt-transport-https lsb-release wget curl \
&& curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg \ 
&& sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' \
&& apt update \
&& apt install --yes --no-install-recommends \
apache2 \
php8.1 \
php8.1-mysql \
php8.1-ldap \
php8.1-xmlrpc \
php8.1-imap \
php8.1-curl \
php8.1-gd \
php8.1-mbstring \
php8.1-xml \
php-cas \
php8.1-intl \
php8.1-zip \
php8.1-bz2 \
php8.1-redis \
cron \
jq \
libldap-2.4-2 \
libldap-common \
libsasl2-2 \
libsasl2-modules \
libsasl2-modules-db \
&& rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite \
&& a2enmod deflate \
&& a2enmod headers \
&& echo "ServerTokens Prod" >> /etc/apache2/apache2.conf \
&& echo "ServerSignature Off" >> /etc/apache2/apache2.conf \
&& sed -i -e 's/session.cookie_httponly =/session.cookie_httponly = on/g' /etc/php/8.1/apache2/php.ini \
&& sed -i -e 's/max_execution_time = 30/max_execution_time = 600/g' /etc/php/8.1/apache2/php.ini

#Exposition des ports
EXPOSE 80 443

CMD ["/usr/sbin/apachectl", "-D", "FOREGROUND"]
